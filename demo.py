import requests
from PIL import Image
from time import sleep, time
from io import BytesIO
from pytesseract import image_to_string
from bs4 import BeautifulSoup


class Bahrain:
	def __init__(self):
		self.session = requests.session()

	def post(self, url, data):
		r = self.session.post(url, data=data)
		if r.ok:
			return r
		raise Exception("Request didn't work.")

	def get(self, url):
		r = self.session.get(url)
		if r.ok:
			return r
		raise Exception("Request didn't work.")

class DataFactory:
	@staticmethod
	def get_visa_selection_form_data(detailed_purpose, gcc_residency, nationality, purpose_of_visit):
		return {
			"ASM": "NEXT",
			"detailedPurpose": "EXHB",
			"detailedPurposeOther": "",
			"gccResidency": "NO",
			"nationality": "IND",
			"nav": "ASM",
			"newExt": "NEW",
			"purposeOfVisit": "BUS",
		}

class DataFinder:
	def __init__(self, html):
		self.soup = BeautifulSoup(html, "html.parser")

	def get_captcha_image_link(self):
		captcha_image = self.soup.find("img", id="captcha_image")
		if captcha_image is not None:
			return captcha_image['src']
		raise Exception("Captcha Image not found.")

	@staticmethod
	def get_captcha_pil_image(binary):
		return Image.open(BytesIO(binary))

	@staticmethod
	def get_captcha_text(pil_image):
		text = image_to_string(pil_image)
		if len(text) > 0:
			return True, text
		return False, text



if __name__ == "__main__":
	start = time()
	base_url = "https://www.evisa.gov.bh"
	visa_url = base_url + "/VISA/visaInput"
	bahrain = Bahrain()

	# start the session.
	print("Starting the session by making the first GET request to apply visa page.")
	bahrain.get(visa_url)

	# make first post.
	data_factory = DataFactory()
	first_form_data = data_factory.get_visa_selection_form_data("EXHB", "NO", "IND", "BUS")
	print("Making the POST request towards next page.")
	resp = bahrain.post(visa_url, first_form_data)
	
	# start preparing for second post.
	print("Finding Captcha Image Link.")
	data_finder = DataFinder(resp.text)
	# although its constant - but this scraping ensures if they change endpoint - the code stays stable.
	captcha_image_relative_link = base_url + data_finder.get_captcha_image_link()
	print("Found Captcha Image Link.")

	# this will be cleaned to its own class, for now just being dirty as prototypical.
	print("Preparing for captcha image retrieval and parsing until we can predict it.")
	status = False
	while status == False:
		resp = bahrain.get(captcha_image_relative_link)
		captcha_image = data_finder.get_captcha_pil_image(resp.content)
		status, captcha_text = data_finder.get_captcha_text(captcha_image)
		print("Couldn't figure captcha... Trying again..")
		sleep(2)
	print("Figured Captcha as code: {0}".format(captcha_text))
	print("Total time taken: {0} seconds".format(round(time()-start), 2))
