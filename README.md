# Prototype

A small POC to see how the workflow might go around scraping and most importantly dealing with captchas in a smart way.

## Installation

1. Create virtualenv: `python3 -m venv scrape-prototype-env`
2. Activate the virtualenv: `source scrape-prototype-env/bin/activate`
3. Update the repository index: `sudo apt-get update`
4. Install the system dependency (OCR to parse captchas): `sudo apt install tesseract-ocr` (Linux)
5. Install the application dependencies: `pip install -r requirements.txt`

## Usage

- Run `python demo.py`

